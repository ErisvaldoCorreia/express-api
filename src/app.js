﻿'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();
const router = express.Router();

// Conexão com o banco de dados
mongoose.connect(
    'mongodb://kamus:eris2015@ds251002.mlab.com:51002/testedb', 
    { useNewUrlParser: true }
);

// Carregamento dos Models
const Product = require('./models/product');

// Carregamento das rotas
const index = require('./routes/index');
const products = require('./routes/products');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// chamada das rotas CRUD
app.use('/', index);
app.use('/products', products);

module.exports = app;