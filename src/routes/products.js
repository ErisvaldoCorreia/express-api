'use strict';

const express = require('express');
const router = express.Router();
const controll = require('../controllers/productControllers');

// Recebe a chamada de requisição dos controllers
router.post('/', controll.post);
router.put('/:id', controll.put);
router.delete('/', controll.delete);

module.exports = router;