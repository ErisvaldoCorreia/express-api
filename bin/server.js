'use strict'

const app = require('../src/app');
const http = require('http');
const debug = require('debug')('nodestr:server');

const port = 3000;
app.set('port', port);

const server = http.createServer(app);

server.listen(port);
server.on('listening', onListening);
console.log('API RODANDO na porta 3000');

function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? 'pip ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}

/*  O uso do http foi realizado pela orientação do curso,
    embora, ao trabalharmos com o express, não seja
    necessario. */